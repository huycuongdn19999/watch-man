// const moveFile = require('../sv24/moveFile')
const fs = require('fs')
const moment = require('moment')

module.exports = async ({ filename, showLog }) => {
  try {
    let data = await fs.readFileSync(filename, 'utf8')
    let receivedAt = ''
    const measureData = data
      .split(`\n`)
      .map(row => {
        /*eslint-disable*/
          if (row.trim() !== '') {
            let rowValues = row
              .replace(`/[^\w\s]/gi`, ' ')
              .replace(/\t/g, '[khoangTrang]')
              .split('[khoangTrang]')
              
            const receivedTime = moment(rowValues[3], 'YYYYMMDDHHmmss')
            /* eslint-enable */
          if (!receivedTime.isValid()) {
            throw new Error('Time not valid')
          }
          if (receivedAt === '') {
            receivedAt = receivedTime.toDate()
          }
          return {
            measureName: rowValues[0],
            value: isNaN(Number.parseFloat(rowValues[1]))
              ? null
              : Number.parseFloat(rowValues[1]),
            unit: rowValues[2],
            time: receivedTime.toDate(),
            statusDevice: rowValues[4]
              ? Number.parseInt(rowValues[4].replace(/[^0-9.]/g, ''))
              : 0
          }
        }
        return null
      })
      .filter(measuring => !!measuring)
    if (showLog) {
      console.info(`${filename} readed`)
    }
    if (!receivedAt) {
      throw new Error('Time not valid')
    }
    return {
      measureData,
      receivedAt
    }
  } catch (e) {
    // moveFile({ filename, type: 'error' })
    console.error(`${filename} not read`, e)
    return { error: true }
  }
}
