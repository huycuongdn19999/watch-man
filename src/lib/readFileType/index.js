const txtFile = require('./txtFile')
const csvFile = require('./csvFile')

const FILE_TYPE = {
  THONG_TU_24: 'TT24',
  CSV_VAG: 'CSV_VAG',
  THONG_TU_5417: 'TT5417'
}

/**
 * Station: {fileType, extensionFile, }
 */
module.exports = ({ fileType, filename, showLog = false }) => {
  switch (fileType) {
    case FILE_TYPE.THONG_TU_24:
      return txtFile({ fileType, filename, showLog })
    case FILE_TYPE.CSV_VAG:
      return csvFile({ fileType, filename, showLog })
    case FILE_TYPE.THONG_TU_5417:
      return txtFile({ fileType, filename, showLog })
    default:
      return { error: true, message: 'File type does not exist!!!!' }
  }
}
module.exports.FILE_TYPE = FILE_TYPE
