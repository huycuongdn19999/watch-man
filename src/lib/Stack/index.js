class Stack extends Array {
  checkCoChua(str) {
    if (this.indexOf(str) === -1) {
      this.push(str);
      if (this.length > 200000) this.shift();
      return false;
    } else {
      return true;
    }
  }
}

module.exports.Stack = Stack;
module.exports = {
  Stack
};
