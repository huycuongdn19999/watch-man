const fs = require('fs');
const path = require('path');
const moment = require('moment');
const _ = require('lodash');
const moveFile = require('./moveFile');
const importData = require('./importData');
const getDataImport = require('./getDataImport');
const readFileType = require('../readFileType');
const { pushNotification } = require('./pushNotification');
const { IS_CHANGE_NAME, IS_MOVE, TYPE } = require('../../config');

const FILE_TYPE = readFileType.FILE_TYPE;

async function changeFileName(filename) {
  let newFileName = path.join(path.dirname(filename), 'IMPORTED_' + path.basename(filename));
  await fs.renameSync(filename, newFileName);
  return newFileName;
}

async function readAndWriteToServer({ filename, station, organization, warningConfigs }) {
  try {
    const extensionFile = station.extensionFile || 'txt';
    // TODO: Sau sẽ lấy từ cấu hình
    const fileType = extensionFile === 'txt' ? FILE_TYPE.THONG_TU_24 : FILE_TYPE.CSV_VAG;
    const { measureData, receivedAt, error, csv } = await readFileType({
      fileType,
      extensionFile,
      filename,
      stationKey: station.stationAuto,
      showLog: false
    });

    if (error) {
      return false;
    }

    let res = null;
    let timeNhan = new Date();

    if (TYPE === 'CSV') {
      const promiseList = [];
      const groupData = _.groupBy(measureData, 'key');

      // NOTE  declare variable cache dataImport
      let dataPushNoti = {};
      //tho
      console.log('measureData: ' + measureData);
      _.mapKeys(groupData, (value, key) => {
        const dataImport = getDataImport({
          station,
          measureData: value,
          receivedAt: moment(key, 'YYYYMMDDHHmmss').toDate(),
          warningConfigs
        });
        timeNhan = moment(key, 'YYYYMMDDHHmmss').toDate();
        dataPushNoti = {
          organization,
          station,
          data: dataImport
        };

        promiseList.push(
          importData({
            stationKey: station.stationAuto,
            data: dataImport
          })
        );
        return key;
      });
      await Promise.all(promiseList);
      res = { success: true };
    } else {
      // console.log(station, "----station---")
      const dataImport = getDataImport({
        station,
        measureData,
        receivedAt,
        warningConfigs
      });
      dataPushNoti = {
        organization,
        station,
        data: dataImport
      };

      timeNhan = receivedAt;

      res = await importData({
        stationKey: station.stationAuto,
        data: dataImport
      });
      // console.log(res.data.lastLog.measuringLogs)
    }

    if (res && res.success) {
      // NOTE  gui du lieu thanh công mới push Notification
      // TODO  gui canh bao
      // console.log(dataPushNoti, "--dataPushNoti--")
      pushNotification(dataPushNoti)

      if (extensionFile === 'csv') {
        let newFilename = filename;
        if (IS_CHANGE_NAME) newFilename = await changeFileName(newFilename);
        if (IS_MOVE) moveFile({ filename: newFilename, type: 'CSV' });
        console.info(`đã xử lý ${filename}`);
      } else {
        let newFilename = filename;
        if (IS_CHANGE_NAME) newFilename = await changeFileName(newFilename);
        if (IS_MOVE) moveFile({ filename: newFilename, type: 'TXT' });
        console.info(`đã xử lý ${filename}`);
      }
    } else {
      console.error(`danger ${filename} - ${JSON.stringify(res)} `.red);
    }

    return res;
  } catch (e) {
    console.error(`ex ${filename} - ${e} `.red);
    return false;
  }
}

module.exports = readAndWriteToServer;
module.exports.getDataImport = getDataImport;
module.exports.readAndWriteToServer = readAndWriteToServer;
module.exports.changeFileName = changeFileName;
