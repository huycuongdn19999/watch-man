// import { checkAndPushNotification } from './../api/MonitoringApi'
const MonitoringApi = require('./../../api/MonitoringApi');

async function pushNotification({ organization, station, data }) {
 
  await MonitoringApi.checkAndPushNotification({ organization, station, data });
}

module.exports.pushNotification = pushNotification;
