const path = require('path');
const fs = require('fs');
const config = require('../../config');
const moment  = require('moment')

module.exports = async function moveFile({ filename, type }) {
  function move(dirPath) {
    // move file
    //pathFile khong co phan root chi co phan sub va ten file \20190704\QN_AA_NUOSXL.txt
    let tamp = filename.split('/')
    if(tamp.length < 2)
      tamp = filename.split('\\')
    let pathFile = tamp[tamp.length - 1]

    const dateStr = moment().format('YYYYMMDD');
    //newImportPath bao gom phan root imported noi voi phan tren tao thanh noi den day du
    //vi du D:\FTP_Imported\Demo\20190704\QN_AA_NUOSXL.txt

    const desPath = `${dirPath}/${dateStr}`;
    let newImportPath = `${desPath}/${pathFile}`

    if (!fs.existsSync(desPath)) {
      fs.mkdirSync(desPath, {recursive: true});
    }
  

    fs.renameSync(filename, newImportPath);
  }

  function moveCSV(dirPath) {
    console.log('move file');
    // move file
    const dateStr = moment().format('YYYYMMDD');
    let tamp = filename.split('/')
    if(tamp.length < 2)
      tamp = filename.split('\\')
    let pathFile = tamp[tamp.length - 1]


    // get root folder import
    // const treeImportReal = newImportPath.replace(path.basename(filename), '')
    const desPath = `${dirPath}/${dateStr}`;
    let newImportPath = `${desPath}/${pathFile}`

    if (!fs.existsSync(desPath)) {
      fs.mkdirSync(desPath, {recursive: true});
    }

    fs.renameSync(filename, newImportPath);
  }
  switch (type) {
    case 'TXT':
      move(config.MOVE_FOLDER, filename);
      break;
    case 'CSV':
      moveCSV(config.MOVE_FOLDER, filename);
      break;
  }
};
