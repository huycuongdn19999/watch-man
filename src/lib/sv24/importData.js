const dataStationAutoDao = require('../../dao/dataStationAutoDao')
const stationAutoDao = require('../../dao/stationAutoDao')

module.exports = async function importData({
  stationKey,
  data: { receivedAt, measuringLogs }
}) {
  try {
    console.log("importData: " + stationKey)
    console.log(measuringLogs, '==measuringLogs===')
    await dataStationAutoDao.createOrUpdate(
      {
        receivedAt,
        measuringLogs
      },
      stationKey
    )
    const resStationAuto = await stationAutoDao.DAOupdateLastLog(
      stationKey,
      { receivedAt, measuringLogs },
    )
    console.log("Done " + resStationAuto)
    return { success: true, data: resStationAuto }
  } catch (e) {
    return { error: true, message: e.message }
  }
}
