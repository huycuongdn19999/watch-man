// const readFileType = require('../readFileType')
const statusDevice = require('../../constants/statusDevice');
const { COLOR_STATUS } = require('../../constants/statusDevice');
const constWarLel = require('../../constants/warningLevels');
const _ = require('lodash');
const { addNotiByOrganizationApi, getDataForNoti } = require('../../api/FcmApi');
const constNoti = require('../../constants/notification');
const CacheNotificationPush = require('../../models/CacheNotificationPush');
const STATUS_STATION = require('../../constants/stationStatus');
const NOTIFICATION_TYPE = require('../../constants/notification');
const moment = require('moment');

// MARK  với mỗi thiết bị sẽ check và gửi cảnh báo cho từng thiết bị
async function checkAndPushStatusDevice({ organization, station, data }) {
  const measuringLogs = _.result(data, 'measuringLogs', {});
  _.mapKeys(measuringLogs, async function(item, measure) {
    // Check deviceStatus
    if (item.statusDevice === statusDevice.ERROR) {
      // get noi dung cho notification
      const contentNoti = getDataForNoti(constNoti.NOTIFICATION_ERROR, {
        station,
        plaintext: `Sensor Error: ${measure}`,
        color: COLOR_STATUS.DIVICE_ERROR
      });

      // check logic
      let isPush = await logicIsPushError(organization, station, measure, item);

      // push notification
      if (isPush) await addNotiByOrganizationApi(organization._id, contentNoti);
    }

    if (item.statusDevice === statusDevice.GOOD) {
      // get noi dung cho notification
      const contentNoti = getDataForNoti(constNoti.NOTIFICATION_ERROR, {
        station,
        plaintext: `Sensor Good again: ${measure}`,
        color: COLOR_STATUS.DIVICE_GOOD
      });

      // check logic
      let isPush = await logicIsPushError(organization, station, measure, item);
      // push notification
      if (isPush) await addNotiByOrganizationApi(organization._id, contentNoti);
    }

    // Check logic
  });
  // console.log('measuringLogs',measuringLogs)
}

async function checkAndPushExceed({ organization, station, data }) {
  // find measure exceed và measure prepare exceed
  const measuringLogs = _.result(data, 'measuringLogs', {});
  const measuringList = _.result(station, 'measuringList', []);
  // console.log('stationstation',station)
  // console.log("measuringLogs", measuringLogs);
  let exceedArr = [];
  let prepareExceedArr = [];
  let dataFilter = [];

  // lọc ra list exceed and prepareExceed
  _.mapKeys(measuringLogs, function(item, measure) {
    if (item.warningLevel === constWarLel.EXCEEDED) {
      exceedArr.push({
        ...item,
        key: measure,
        conf: measuringList.find(mea => mea.measuringDes === measure),
        unitStr: getLimitText(item)
      });

      dataFilter.push(measure);
    }

    if (item.warningLevel === constWarLel.EXCEEDED_PREPARING) {
      prepareExceedArr.push({
        ...item,
        key: measure,
        conf: measuringList.find(mea => mea.measuringDes === measure),
        unitStr: getLimitText(item)
      });
      dataFilter.push(measure);
    }
  });

  if (exceedArr.length > 0 || prepareExceedArr.length > 0) {
    // NOTE  logic Push
    let isPush = await logicIsPushExceeded(organization, station, dataFilter, exceedArr.length > 0);

    if (!isPush) return;

    // get content
    const contentNoti = getDataForNoti(constNoti.NOTIFICATION_EXCEEDED, {
      station,
      exceedArr,
      prepareExceedArr,
      dataFilter
    });

    // push notification
    await addNotiByOrganizationApi(organization._id, contentNoti);
  }
}

async function logicIsPushExceeded(organization, station, dataFilter, isExceeded) {
  try {
    let cacheNotiPush;
    cacheNotiPush = await CacheNotificationPush.findOne({
      organizationId: organization._id,
      stationId: station._id,
      type: NOTIFICATION_TYPE.NOTIFICATION_EXCEEDED
    });

    if (!cacheNotiPush) {
      // NOTE  isPush =true, sau đó save vào database
      await CacheNotificationPush.create({
        organizationId: organization._id,
        stationId: station._id,
        measureList: dataFilter,
        type: NOTIFICATION_TYPE.NOTIFICATION_EXCEEDED,
        timePush: moment().toDate(),
        status: isExceeded ? STATUS_STATION.EXCEEDED : STATUS_STATION.EXCEEDED_PREPARING
      });
      return true;
    }
    // 2.	Chuyển từ trạng thái Chuẩn bị vượt sang Vượt
    if (cacheNotiPush.status === STATUS_STATION.EXCEEDED_PREPARING && isExceeded) {
      await CacheNotificationPush.findOneAndUpdate(
        { _id: cacheNotiPush._id },
        {
          $set: {
            timePush: moment().toDate(),
            measureList: _.union(cacheNotiPush.measureList, dataFilter),
            status: STATUS_STATION.EXCEEDED
          }
        }
      );

      return true;
    }

    // 3.	Tăng thêm chỉ tiêu vượt ngưỡng
    const measureMerge = _.union(cacheNotiPush.measureList, dataFilter);
    if (!_.isEqual(cacheNotiPush.measureList, measureMerge)) {
      let status = cacheNotiPush.status || isExceeded ? STATUS_STATION.EXCEEDED : STATUS_STATION.EXCEEDED_PREPARING;
      await CacheNotificationPush.findOneAndUpdate(
        { _id: cacheNotiPush._id },
        {
          $set: {
            timePush: moment().toDate(),
            measureList: _.union(cacheNotiPush.measureList, dataFilter),
            status: status
          }
        }
      );
      return true;
    }

    return false;
  } catch (e) {
    console.log('Lỗi logicIsPushExceeded:', e);
  }
}

async function logicIsPushError(organization, station, measure, device) {
  try {
    let cacheNotiPush = await CacheNotificationPush.findOne({
      organizationId: organization._id,
      stationId: station._id,
      type: NOTIFICATION_TYPE.NOTIFICATION_ERROR,
      measure
    });

    if (!cacheNotiPush) {
      // NOTE nếu lần đầu GOOD thì k0 gửi
      if (device.statusDevice === statusDevice.GOOD) return false;

      await CacheNotificationPush.create({
        organizationId: organization._id,
        stationId: station._id,
        measure,
        type: NOTIFICATION_TYPE.NOTIFICATION_ERROR,
        status: device.statusDevice
      });
      return true;
    }

    // MARK cacheNotiPush.status: String, device.statusDevice: Number =>  so sánh giá trị
    if (cacheNotiPush.status != device.statusDevice) {
      await CacheNotificationPush.findOneAndUpdate(
        { _id: cacheNotiPush._id },
        {
          $set: {
            status: device.statusDevice
          }
        }
      );
      return true;
    }

    return false;
  } catch (e) {
    console.log('lỗi logicIsPushError:', e);
    return false;
  }
}

async function pushNotification({ organization, station, data }, timeNhan) {
  let isPush = _.result(station, 'options.warning.allowed', false);
  const dataFrequency = _.result(station, 'dataFrequency', 0);

  if (isPush) {
    //isPush = moment().isBefore(moment(timeNhan).add(dataFrequency * 3, 'minutes'));
  }

  if (isPush) {
    // console.log("station", station);
    checkAndPushStatusDevice({ organization, station, data });

    checkAndPushExceed({ organization, station, data });
  }

  // NOTE  stationKey: station.stationAuto,
  // NOTE  organizationId: organization._id

  // NOTE  check if statusDevice == 02 bi loi
}

// 00 đang đo
// 01 hiệu chuẩn
// 02 báo lỗi thiết bị

function getLimitText(measure) {
  const { unit } = measure;
  let minLimit = _.get(measure, 'minLimit', null);
  let maxLimit = _.get(measure, 'maxLimit', null);
  if (minLimit === '') minLimit = null;
  if (maxLimit === '') maxLimit = null;

  if (minLimit !== null && maxLimit !== null) {
    return `(${minLimit} -> ${maxLimit}${unit ? ' ' + unit : ''})`;
  }

  if (minLimit !== null) {
    return `(> ${minLimit}${unit ? ' ' + unit : ''})`;
  }

  if (maxLimit !== null) {
    return `(< ${maxLimit}${unit ? ' ' + unit : ''})`;
  }

  return ``;
}

module.exports.pushNotification = pushNotification;
