const mongoose = require('mongoose');
const { DB_ADMIN_URI, DB_ADMIN_USER, DB_ADMIN_PASS } = require('../config');

var conn = mongoose.createConnection(DB_ADMIN_URI, {
  user: DB_ADMIN_USER,
  pass: DB_ADMIN_PASS,
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 5000 // Reconnect every 500ms
});

conn.on('error', err => console.log(err));
conn.once('open', () => {
  console.log(DB_ADMIN_URI + ' connect success full');
});

module.exports = exports = conn;
