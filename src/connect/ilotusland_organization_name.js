const mongoose = require('mongoose');
const { DB_ORG_URI, DB_ORG_USER, DB_ORG_PASS } = require('../config');

//TODO
var conn = mongoose.createConnection(DB_ORG_URI, {
  user: DB_ORG_USER,
  pass: DB_ORG_PASS,
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 5000 // Reconnect every 500ms
});

// const db = mongoose.connection;

conn.on('error', err => console.log(err));

conn.once('open', () => {
  console.log(DB_ORG_URI + ' connect success full');
});

module.exports = exports = conn;
