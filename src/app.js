
const _ = require('lodash');
require('colors');
const schedule = require('node-schedule');

// const { WathmanCustom } = require('./lib/watchman');
const { Stack } = require('./lib/Stack');
const { getSysConfig } = require('./api/CategoryApi');
const sv4ReadAndWriteToServer = require('./lib/sv24/readFile');

const Organization = require('./models/Organization');
// const StationAuto = require('./models/StationAuto')
const stationAutoDao = require('./dao/stationAutoDao');
const watch = require('node-watch');
const { ROOT_WATCH, DB_ORG_NAME, TYPE } = require('./config');

const rootFTP = ROOT_WATCH;
const typeFile_toUpper = _.toUpper(TYPE);

const stack = new Stack();

main();
async function main() {
  await loadData();
  watch(
    rootFTP,
    {
      recursive: true,
      filter: (f) => {
        console.log('run 1', typeFile_toUpper);
        if (typeFile_toUpper == 'TXT') {
          return /^.*\.txt$/.test(f) || /^.*\.TXT$/.test(f);
        } else if (typeFile_toUpper == 'CSV') {
          return /^.*\.csv$/.test(f) || /^.*\.CSV$/.test(f);
        }
      },
    },
    function (evt, fileNameWithPath) {
      if (evt == 'remove') {
        return;
      }
      console.log(`file changed: ${fileNameWithPath}`.green);
      if (!stack.checkCoChua(fileNameWithPath)) {
        console.log('lamviec', stack.length);
      } else {
        console.log('k0 co lam', stack.length);
      }
    }
  );

  schedule.scheduleJob('* * * * * *', async () => {
    try {
      console.log(`Xử lý File Starting !!! stack:${stack.length}`.green);
      if (stack.length > 0) lamviec(stack.shift());
    } catch (e) {
      console.log(`Đây là lỗi: ${JSON.stringify(e)}`.red);
    }
  });

  schedule.scheduleJob('*/5 * * * *', async () => {
    try {
      console.log(`LOADINGGGG !!!`.green);
      loadData();
    } catch (e) {
      console.log(`Đây là lỗi: ${JSON.stringify(e)}`.red);
    }
  });
}

async function lamviec(fileNameWithPath = '') {
  let fileNames = fileNameWithPath.split('/');
  if (fileNames.length < 2) fileNames = fileNameWithPath.split('\\');
  const fileLastName = fileNames[fileNames.length - 1];

  let station;

  if (TYPE === 'TXT') {
    const tampArr = fileLastName.split('_');
    console.log('tampArr', tampArr);

    if (tampArr.length >= 2) {
      tampArr.pop();
      let fileStationName = tampArr.join('_');
      console.log('fileStationName', fileStationName);
      station = global.dataStations.find((station) => {
        return fileStationName === station.fileName;
      });
    } else {
      console.log('FOLDER: FILE SAI DINH DANG'.red, fileLastName);
      return;
    }
  } else {
    station = global.dataStations.find((station) => {
      return fileLastName.indexOf(station.fileName) > -1;
    });
  }

  // Error if file not match with station.path config
  if (!station) {
    console.log('FOLDER: Not find folder'.red, fileLastName);
    return;
  }

  //const realFilePath = path.join(rootFTP, fileNameWithPath);
  const realFilePath = fileNameWithPath;
  console.log('READ_FILE_PATH', realFilePath);

  try {
    sv4ReadAndWriteToServer({
      filename: realFilePath,
      organization: global.organization,
      station,
      warningConfigs: global.warningConfigs,
    });
  } catch (e) {
    console.log(`loi nè ${e}`.red);
  }
}

async function loadData() {
  const sysConfig = await getSysConfig();
  const warningConfigs = _.result(sysConfig, 'data.stationAuto.warning', null);
  if (!warningConfigs) throw new Error('warningConfigs đang k0 load dc');
  const organization = await Organization.findOne({
    'databaseInfo.name': DB_ORG_NAME, //TODO
  }).lean();

  const dataStations = await stationAutoDao.DAOgetConfiguredLogger();

  global.sysConfig = sysConfig;
  global.warningConfigs = warningConfigs;
  global.organization = organization;
  global.dataStations = dataStations;
}
