module.exports = {
  GOOD: 0,
  HIEU_CHUAN: 1,
  ERROR: 2
}

const COLOR_STATUS = {
  DATA_LOSS: '#666666',
  EXCEEDED: '#CC0000',
  EXCEEDED_PREPARING: '#E69138',
  GOOD: '#6AA84F',

  DIVICE_ERROR: '#CC0000',
  DIVICE_GOOD: '#6AA84F'

}

module.exports.COLOR_STATUS = COLOR_STATUS
