const getDataStationAuto = require('../models/getDataStationAuto');

module.exports = {
  async createOrUpdate({ receivedAt, measuringLogs }, stationKey) {
    const Model = getDataStationAuto(stationKey);
    const item = await Model.findOneAndUpdate(
      { receivedAt },
      {
        $set: {
          receivedAt,
          measuringLogs,
          createdAt: Date.now(),
          updatedAt: Date.now()
        }
      },
      { upsert: true, new: true, runValidators: true }
    );
    return item;
  }
};
