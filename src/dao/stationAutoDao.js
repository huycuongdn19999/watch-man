const StationAuto = require('../models/StationAuto')

async function DAOgetConfiguredLogger(query = {}) {

  let listItem = await StationAuto.find(
    {
      $and: [{ configLogger: { $exists: true } }, query]
    },
    {
      _id: 1,
      name: 1,
      key: 1,
      measuringList: 1,
      configLogger: 1,
      'options.approve': 1,
      'options.warning': 1,
      dataFrequency: 1
    }
  )
  return listItem.map(itemConfigLoggerFilter).filter(item => !!item)
}

async function DAOupdateLastLog(key, { receivedAt, measuringLogs }) {
  console.log("Dao last log: " + JSON.stringify({ receivedAt, measuringLogs }, null, 4))
  let stationAuto = await StationAuto.findOne({ key })
  if (!stationAuto) return false
  let lastLogDB = stationAuto.lastLog
  // console.log(lastLogDB, '==lastLogDB==')
  let receivedAtClient = new Date(receivedAt)
  if (lastLogDB && lastLogDB.receivedAt) {
    let receivedAtDB = new Date(lastLogDB.receivedAt)
    // Neu du lieu gui ve la thoi gian truoc do => khong update vao lastLog
    if (receivedAtDB > receivedAtClient) return null
  }
  let item = await StationAuto.findOneAndUpdate(
    { key },
    {
      $set: {
        lastLog: { receivedAt, measuringLogs }
      }
    },
    { new: true }
  )
  return item
}

const itemConfigLoggerFilter = item => {
  let measuringList = item.measuringList
  let configLogger = item.configLogger
  const options = item.options || {}
  if (
    configLogger &&
    measuringList &&
    configLogger.fileName &&
    configLogger.path &&
    configLogger.measuringList
  ) {
    const mList = []
    let measuringLogger = configLogger.measuringList
    measuringList.map(m => {
      measuringLogger.map(mLogger => {
        if (mLogger.measuringDes && mLogger.measuringDes === m.key) {

          mList.push({
            ...mLogger,
            minLimit: m.minLimit,
            maxLimit: m.maxLimit,
            maxTend: m.maxTend,
            minTend: m.minTend,
            unit: m.unit
          })
        }
      })
    })
    if (mList.length > 0) {
      return {
        _id: item._id,
        stationName: item.name,
        stationAuto: item.key,
        fileName: configLogger.fileName,
        path: configLogger.path,
        extensionFile: configLogger.extensionFile || 'txt',
        measuringList: mList,
        options
      }
    }
  }
  return null
}

module.exports = exports = {
  ...StationAuto,
  DAOgetConfiguredLogger,
  DAOupdateLastLog
}