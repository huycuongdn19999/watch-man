const { ilotusland_organization_name } = require('../connect');
const mongoose = require('mongoose');

const StationAuto = new mongoose.Schema({
  key: String,
  name: String,
  stationType: Object,
  address: String,
  mapLocation: Object,
  emails: Object,
  phones: Object,
  options: Object,
  measuringList: Object,
  lastLog: Object,
  image: Object,
  configLogger: {
    type: Object,
    default: { fileName: '', path: '', measuringList: [] }
  },
  removeStatus: {
    type: Object,
    default: { allowed: false, removeAt: null }
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  dataFrequency: Number
});

module.exports = ilotusland_organization_name.model('station-autos', StationAuto);
