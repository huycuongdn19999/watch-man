const { ilotusland_organization_name } = require('../connect');
const mongoose = require('mongoose')

const prefix = 'data-station-';

const DataStationAuto = new mongoose.Schema({
  receivedAt: { type: Date, default: Date.now },
  measuringLogs: Object,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
});

module.exports.prefix = prefix;
module.exports = function createDataModel(stationKey) {
  const tableName = prefix + stationKey;
  if (ilotusland_organization_name.models && ilotusland_organization_name.models[tableName]) {
    return ilotusland_organization_name.models[tableName];
  } else {
    return ilotusland_organization_name.model(tableName, DataStationAuto);
  }
};
